import React, {useState} from 'react';
import DatePicker from 'react-datepicker';
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import "react-datepicker/dist/react-datepicker.css";

const DatePickerForm = () => {
	const [startDate, setStartDate] = useState(new Date());

	return (
		<React.Fragment>
			<DatePicker 
				selected={startDate} 
				onChange={date => setStartDate(date)} 
			/>
		</React.Fragment>
	);

};


export default DatePickerForm;

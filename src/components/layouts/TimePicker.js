import React, {useState} from 'react';
import DatePicker from 'react-datepicker';
import setHours from "date-fns/setHours";
import setMinutes from "date-fns/setMinutes";
import "react-datepicker/dist/react-datepicker.css";

const TimePickerForm = () => {
	const [startDate, setStartDate] = useState();

	return (
		<React.Fragment>
			<DatePicker
				selected={startDate}
				onChange={date => setStartDate(date)}
				showTimeSelect
				showTimeSelectOnly
				minTime={setHours(setMinutes(new Date(), 0), 11)}
				maxTime={setHours(setMinutes(new Date(), 59), 23)}
				timeIntervals={60}
				dateFormat="h:mm aa"
			/>
		</React.Fragment>
	);

};

export default TimePickerForm;

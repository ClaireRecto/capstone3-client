import React, {Fragment} from 'react';

const Footer = () => {
	return(
		<Fragment>
			<footer className="pt-3">
				<small className="">All rights reserved &copy; 2020 Clariza Recto | Capstone III</small>
			</footer>
		</Fragment>
	)
}

export default Footer;
import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';

const Navbar = () => {
	const handleLogout =() => {
		// e.preventDefault();
			// if(localStorage.getItem('user')){
				localStorage.removeItem('role');
				localStorage.removeItem('user');
				localStorage.removeItem('token');
				localStorage.removeItem('isLogged');

				window.location.href = "/"
			// } 
		
	}
	return(
		<Fragment>
			<nav className="navbar navbar-expand-sm">
				<Link to="/" className="navbar-brand"><img src="https://scontent.fmnl6-1.fna.fbcdn.net/v/t1.15752-9/85201247_2558222097838251_4520499627895554048_n.png?_nc_cat=107&_nc_eui2=AeGadiVfZwd-Q6k-amWAnTIgdsCJptcfXSlo4gwxXcB7YImiBkagX466qfu2OKe9Yf61eaQ2BnX8_PWGtdTUSNNHs-cFTP-Gj8EzWtQ0x_Mw2g&_nc_ohc=rPQVHUupXqgAX_cBc8C&_nc_ht=scontent.fmnl6-1.fna&oh=3db1d7306659a727abf0f81cac6b0263&oe=5ED3E467" alt="" id="brand-logo"/></Link>
				<button 
					className="navbar-toggler navbar-light "
					type="button"
					data-toggle="collapse"
					data-target="#navbarNav">
				<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarNav">
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<Link to="/rooms" className="nav-link">OUR ROOMS</Link>
						</li>
						{
							localStorage.getItem('role') == 'admin' ?
							<Fragment>
								<li className="nav-item">
									<Link to="/admin" className="nav-link">ADMIN PANEL</Link>
								</li>
							</Fragment> : 
							<Fragment>
								<li className="nav-item">
									<Link to="/booking" className="nav-link">BOOK A ROOM!</Link>
								</li>
							</Fragment>
						}

						
						
						{
							localStorage.getItem('isLogged', true) ? 
							<Fragment>
							<li className="nav-item">
									<Link to="/transactions" className="nav-link">TRANSACTIONS</Link>
								</li>
							<li className="nav-item">
								<Link to="/" className="nav-link">WELCOME,{localStorage.getItem('user')}</Link>
							</li>
							<li className="nav-item">
								<Link to="/" className="nav-link" onClick={handleLogout}>LOGOUT</Link>
							</li>
							</Fragment>	 : 
							<Fragment>
								<li className="nav-item">
									<Link to="/register" className="nav-link">REGISTER</Link>
								</li>
								<li className="nav-item">
									<Link to="/login" className="nav-link">LOGIN</Link>
								</li>
							</Fragment>	
						}
						
					</ul>
				</div>
			</nav>
		</Fragment>
	);
}

export default Navbar;

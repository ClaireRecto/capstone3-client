import React, {useState} from 'react';

const EditCategory = ({categories, handleChangeCategoriesStatus, categoriesStatus}) => {
	const [selectedCategory,setSelectedCategory] = useState({
		name: null,
		id : null
	})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find(category=>{
			return category._id === e.target.value
		})

		setSelectedCategory({
			id: e.target.value,
			name : categorySelected.name

		})
	}

	const handleChangeName = e => {
		setSelectedCategory({
			...selectedCategory,
			[e.target.name] : e.target.value
		})
	}

	const handleEditCategory = e => {
		e.preventDefault();
		fetch("https://bookingking.herokuapp.com/categories/"+selectedCategory.id,{
			method: "PUT",
			body : JSON.stringify({name : selectedCategory.name}),
			headers : {
				"Content-Type" : "application/json",
				Authorization : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => {
			handleChangeCategoriesStatus({
				lastUpdated : selectedCategory.id,
				status : "pass",
				isLoading : true
			});
			window.location.href = "/admin"
		})
	}
	return(
		<form action="" onSubmit={handleEditCategory}>
			<label htmlFor="category">Select Category Name:</label>
			{
				categoriesStatus.isLoading ? 
					<div class="spinner-border" role="status">
					  <span class="sr-only">Loading...</span>
					</div> : 
					<React.Fragment>
						<select onChange={handleChangeSelected} name="category" id="category" className="custom-select">
							<option value="" disabled selected>Select Category</option>
							{
								categories.map(category => {
									return (
									<option value={category._id}>{category.name}</option>
									)
								})
							}
						</select>
						<label htmlFor="category-update">Update into:</label>
						<input type="text" className="form-control" name="name" id="category-update" onChange={handleChangeName} value={selectedCategory.name} />
						<button className="btn mt-2 w-100">EDIT</button>
					</React.Fragment>
			}

		</form>
	)
}

export default EditCategory;

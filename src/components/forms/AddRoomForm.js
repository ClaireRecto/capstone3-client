import React,{useState} from 'react';

const AddRoom = ({categories}) => {
	const [room, setRoom] = useState({
		name : null,
		description : null,
		price : null,
		capacity : null,
		categoryId : null,
		image : null
	})

	const handleChangeText = e => {
		setRoom({
			...room,
			[e.target.name] : e.target.value
		})
	}

	const handleChangeFile = e => {
		setRoom({
			...room,
			image : e.target.files[0]
		})
	}

	const handleAddRoom = e => {
		e.preventDefault();
		const formData = new FormData();
		formData.append('name', room.name)
		formData.append('price', room.price)
		formData.append('capacity', room.capacity)
		formData.append('description', room.description)
		formData.append('categoryId', room.categoryId)
		formData.append('image', room.image)

		console.log(formData.get('name'))
		console.log(formData.get('price'))
		console.log(formData.get('capacity'))
		console.log(formData.get('description'))
		console.log(formData.get('categoryId'))
		console.log(formData.get('image'))

		fetch("https://bookingking.herokuapp.com/rooms",{
			method: "POST",
			body: formData,
			headers: {

				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(room => {console.log(room);window.location.href = "/admin"})
	}


	return(
		<form action="" onSubmit={handleAddRoom} enctype="multipart/form-data">
			<label htmlFor="name-add">Name:</label>
			<input type="text" className="form-control" name="name" id="name-add" onChange={handleChangeText}/>
			<label htmlFor="price-add">Price:</label>
			<input type="number" className="form-control" name="price" id="price-add" onChange={handleChangeText}/>
			<label htmlFor="category-add">Category</label>
			<select name="categoryId" id="category-add" className="form-control" onChange={handleChangeText}>
				<option value="" disabled selected>Select Category</option>
				{
					categories.map(category => {
						return (
						<option value={category._id}>{category.name}</option>
						)
					})
				}
			</select>
			<label htmlFor="capacity">Capacity:</label>
			<input type="number" className="form-control" name="capacity" id="capacity" onChange={handleChangeText}/>
			<label htmlFor="image-add">Image:</label>
			<input type="file" className="form-control-file" name="image" id="image-add" onChange={handleChangeFile}/>
			<label htmlFor="description-add">Description:</label>
			<textarea name="description" id="description-add" cols="30" rows="10" className="form-control" onChange={handleChangeText}></textarea>
			<button className="btn mt-2 w-100">ADD</button>
		</form>
	)
}

export default AddRoom;

import React, {useState,Fragment} from 'react';

const BookForm = ({rooms}) => {
  const [transaction, setTransaction] = useState({
    reservationDate : null,
    timeIn : null,
    timeOut : null,
    roomId : null,
    paymentMode : null
  });

  const {reservationDate, timeIn, timeOut, roomId, paymentMode} = transaction;

  const handleChangeText = e => {
    setTransaction({
      ...transaction,
      [e.target.name] : e.target.value
    });
  }

  const [selectedRoom,setSelectedRoom] = useState({
    name : null,
    price : null,
    description : null,
    capacity : null,
    image : null,
    id : null
  })

  const handleChangeSelected = e => {
    let roomselected = rooms.find(room=>{
      return room._id === e.target.value
    })

    setSelectedRoom({
      id: e.target.value,
      name : roomselected.name,
      price : roomselected.price,
      capacity : roomselected.capacity,
      description : roomselected.description,
      image : roomselected.image
    })
  }

  const handleOnClick = e => {
    setTransaction({
      ...transaction,
      roomId : selectedRoom.id
    })
  }

  const handleCheckout = e => {
    e.preventDefault();
    let data = {
      reservationDate : transaction.reservationDate,
      timeIn : transaction.timeIn,
      timeOut : transaction.timeOut,
      roomId : transaction.roomId,
      paymentMode : transaction.paymentMode
    };

    fetch("https://bookingking.herokuapp.com/transactions",{
      method : "POST",
      body : JSON.stringify(data),
      headers : {
        "Content-Type" : "application/json",
        "Authorization" : localStorage.getItem('token')
      }
    })
    .then(data => data.json())
    .then(result => {
      console.log(result);
      window.location.href = "/transactions";

    })
  }

	return (
		<div className="container">
      <div className="row">
        <div className="col-12 mx-auto mt-3">
          <h2 className="text-center">BOOK A ROOM!</h2>
          <hr/>
        </div>
      </div>
      {
        localStorage.getItem('isLogged', true) ?
        <Fragment>
          <div className="row rounded admindiv">
            <div className="col">
              <div className="row ">
                <div className="col-12 col-md-6 p-5">
                  
                  {
                    selectedRoom.id ?
                        <div className="card">
                          <img src={"https://bookingking.herokuapp.com"+ selectedRoom.image} alt="" className="img-fluid w-100"/>
                          <div className="card-body">
                            <h3 className="card-text">{selectedRoom.name}</h3>
                            <p className="card-text">MAX {selectedRoom.capacity} PAX</p>
                            <p className="card-text">&#8369; {selectedRoom.price}/HOUR</p>
                            <p className="card-text">{selectedRoom.description}</p>
                          </div>
                        </div> : 
                        <div>
                          <h5 className="text-center">Selected room details will appear here...</h5>
                        </div>
                   
                  }
                  
                </div>
                <div className="col-12 col-md-6 p-5">
                  <label htmlFor="roomId">Room:</label>
                    <div className="row">
                      <div className="col-8">
                        <select name="roomId" id="roomId" className="form-control" onChange={handleChangeSelected}>
                          <option value="" disabled selected>Select Room</option>
                          {
                            rooms.map(room => {
                              return (
                              <option value={room._id}>{room.name}</option>
                              )
                            })
                          }
                        </select>

                      </div>
                      <div className="col-4">
                        <button className="btn w-100" name="roomId" onClick={handleOnClick}>I want this!</button>
                      </div>
                    </div>
                  <label htmlFor="reservationDate">Date:</label>
                  <input type="date" className="form-control" name="reservationDate" id="reservationDate" onChange={handleChangeText} />
                  <label htmlFor="timeIn">Time In:</label>
                  <input type="time" className="form-control" name="timeIn" id="timeIn" onChange={handleChangeText} />
                  <label htmlFor="timeOut">Time Out:</label>
                  <input type="time" className="form-control" onChange={handleChangeText} name="timeOut" id="timeOut"  />
                  <label htmlFor="paymentMode">Payment Method:</label>
                  <select name="paymentMode" id="paymentMode" className="form-control" onChange={handleChangeText} 
                  >
                    <option value="" selected disabled>Select Payment Method </option>
                    <option value="Credit Card">Credit Card</option>
                    <option value="Debit Card">Debit Card</option>
                    <option value="GCash">GCash</option>
                    <option value="PayPal">PayPal</option>
                  </select>
                </div>
              </div>
              {
                transaction.roomId ?
                  <div className="row">
                    <div className="col-10 mx-auto text-center">
                      <h4>BOOKING DETAILS</h4>
                      <table className="table">
                        
                        <tr>
                          <th className="text-right">Room:</th>
                          <td className="text-left">
                          {
                            roomId === selectedRoom.id ? 
                            selectedRoom.name : ""
                          }
                          </td> 
                          
                          
                        </tr>
                        <tr>
                          <th className="text-right">Date:</th>
                          <td className="text-left">{reservationDate}</td>
                        </tr>
                        <tr>
                          <th className="text-right">Time:</th>
                          <td className="text-left">{timeIn}-{timeOut}</td>
                        </tr>
                        <tr>
                          <th className="text-right">Total Hours:</th>
                          <td className="text-left">out - in</td>
                        </tr>
                        <tr>
                          <th className="text-right">Total Price:</th>
                          <td className="text-left">{selectedRoom.price}</td>
                        </tr>
                        <tr >
                          <th className="w-50 text-right">Payment Method:</th>
                          <td className="text-left">{paymentMode}</td>
                        </tr>
                      </table>

                      <button onClick={handleCheckout} className="btn w-50 mx-auto mb-3">BOOK</button>
                    </div>

                  </div> : ""
              }


            </div>


          </div>
        </Fragment> : 
        <Fragment>
          <div className="row rounded admindiv">
            <div className="col-5 mx-auto">
              <h4 className="text-center">PLEASE <a href="/login">LOGIN</a> TO PROCEED</h4>
              
            </div>
          </div>
        </Fragment>
      }

        

    </div>
	);
};


export default BookForm;



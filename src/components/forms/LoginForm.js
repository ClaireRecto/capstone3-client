import React, {useState} from 'react';

const LoginForm = () => {
	const [formData,setFormData] = useState({
		email : null,
		password : null
	});

	const [result,setResult] = useState({
		message : null,
		successful : null
	})


	const handleChange = e => {
		setFormData({
			...formData,
			[e.target.name] : e.target.value
		})
	};

	const handleLogin = e => {
		e.preventDefault();
		let data = {
			email : formData.email,
			password: formData.password,
		}
		fetch("https://bookingking.herokuapp.com/users/login",{
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type" : "application/json"
			}
		})
		.then(data => data.json())
		.then(user => {
			console.log(user)
			if(user.token){
				setResult({
					successful : true,
					message: user.message
				})
				localStorage.setItem('role',(user.user.role));
				localStorage.setItem('user',(user.user.firstname));
				localStorage.setItem('token', "Bearer "+user.token);
				localStorage.setItem('isLogged', true);
			} else {
				setResult({
					successful : false,
					message: user.message
				})
			}
			window.location.href = "/"
		})
	}

	const resultMessage = () => {
		let classList;
		if(result.successful === true){
			classList = 'alert alert-success'
		} else {
			classList = 'alert alert-warning'
		}
		return(
			<div className={classList}>
				{result.message}
			</div>
		)
	}
	return(
		<div className="container">
			<div className="row">
				<div className="col-12 mx-auto mt-3">
					<h2 className="text-center">LOGIN</h2>
					<hr/>
				</div>
			</div>
			<div className="row">
				<div className="col-lg-6 mx-auto pt-4">
					<div className="alert alert-danger d-none" role="alert" id="message"></div>
					<div className="alert alert-success d-none" role="alert" id="smessage"></div>
					{result.successful === null ? "" : resultMessage()}
					<form className="admindiv rounded p-3" onSubmit={handleLogin}>
						<div className="form-group">
							<label htmlFor="email">Email:</label>
							<input 
								type="email"
								className="form-control"
								id="email"
								name="email"
								onChange={handleChange}
								
							/>
						</div>
						<div className="form-group">
							<label htmlFor="password">Password:</label>
							<input 
								type="password"
								className="form-control"
								id="password"
								name="password"
								onChange={handleChange}
							/>
						</div>
						<button type="submit" className="btn w-100">SUBMIT</button>
					</form>	
				</div>
			</div>
		</div>
	)
}

export default LoginForm;

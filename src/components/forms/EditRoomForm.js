
import React, {useState} from 'react';

const EditRoom = ({categories,rooms}) => {
	const [selectedRoom,setSelectedRoom] = useState({
		name : null,
		price : null,
		description : null,
		capacity : null,
		image : null,
		categoryId : null,
		id : null
	})

	const handleChangeSelected = e => {
		let roomselected = rooms.find(room=>{
			return room._id === e.target.value
		})

		setSelectedRoom({
			id: e.target.value,
			name : roomselected.name,
			price : roomselected.price,
			capacity : roomselected.capacity,
			description : roomselected.description,
			categoryId : roomselected.categoryId,
			image : roomselected.image
		})
	}

	const handleChangeName = e => {
		setSelectedRoom({
			...selectedRoom,
			[e.target.name] : e.target.value
		})
	}

	const handleChangeFile = e => {
		setSelectedRoom({
			...selectedRoom,
			image : e.target.files[0]
		})

		console.log(selectedRoom)
	}

	const handleEditRoom = e => {
		e.preventDefault();
		const formData = new FormData();
		formData.append('name', selectedRoom.name)
		formData.append('price', selectedRoom.price)
		formData.append('description', selectedRoom.description)
		formData.append('capacity', selectedRoom.capacity)
		formData.append('categoryId', selectedRoom.categoryId)
		
		if (typeof selectedRoom.image === 'object') {
			formData.append('image',selectedRoom.image)
		}

		console.log(formData.get('image'))

		fetch("https://bookingking.herokuapp.com/rooms/"+selectedRoom.id,{
			method: "PUT",
			body: formData,
			headers: {

				"Authorization" : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(room => {console.log(room);window.location.href = "/admin"})
	}
	return(
		<form action="" onSubmit={handleEditRoom}>
			<select name="category" id="selectroom-delete" className="custom-select" onChange={handleChangeSelected}>
				<option value="" disabled selected>Select room</option>
				{
					rooms.map(room => {
						return (
						<option value={room._id}>{room.name}</option>
						)
					})
				}
			</select>



			<img src={"https://bookingking.herokuapp.com"+ selectedRoom.image} alt="" className="d-block mx-auto mt-2 img-fluid"/>
			<input type="file" className="form-control-file" name="image" id="image-add" onChange={handleChangeFile}/>
			<label htmlFor="name-edit">Name:</label>
			<input type="text" className="form-control" name="name" id="name-edit" value={selectedRoom.name} onChange={handleChangeName}/>
			<label htmlFor="price-edit">Price:</label>
			<input type="number" className="form-control" name="price" id="price-edit" value={selectedRoom.price} onChange={handleChangeName}/>
			<label htmlFor="capacity-edit">Capacity:</label>
			<input type="number" className="form-control" name="capacity" id="capacity-edit" value={selectedRoom.capacity} onChange={handleChangeName}/>
			<label htmlFor="category-edit">Category</label>

			<select name="categoryId" id="category-edit" className="form-control" onChange={handleChangeName} >
				<option value="" disabled selected>Select Category</option>
				{
					categories.map(category => {
						return (
							category._id === selectedRoom.categoryId ? 
							<option value={category._id} selected>{category.name}</option>
							:
							<option value={category._id} >{category.name}</option>
						)
					})
				}


			</select>
			<label htmlFor="description-edit">Description:</label>
			<textarea name="description" id="description-edit" cols="30" rows="10" className="form-control" value={selectedRoom.description} onChange={handleChangeName}></textarea>
			<button className="btn mt-2 w-100">EDIT</button>
		</form>
	)
}

export default EditRoom;

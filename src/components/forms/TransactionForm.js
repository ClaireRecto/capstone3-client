import React, {useState,Fragment} from 'react';


const TransactionForm = ({transactions}) => {
	const [selectedTransaction,setSelectedTransaction] = useState({
			state : null
	})

	const handleChangeSelected = e => {
		let transactionselected = transactions.find(transaction=>{
			return transaction._id === e.target.value
		})

		setSelectedTransaction({
			status : transactionselected.status
		})
	}

	const handleChangeName = e => {
		setSelectedTransaction({
			...selectedTransaction,
			[e.target.name] : e.target.value
		})
	}

	const handleEditTransaction = e => {
		e.preventDefault();
		fetch("https://bookingking.herokuapp.com/transactions/"+selectedTransaction.id,{
			method: "PUT",
			body : JSON.stringify({status : selectedTransaction.status}),
			headers : {
				"Content-Type" : "application/json",
				Authorization : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => {
			console.log(result);
			window.location.href = "/transactions"
		})
	}

	return(
		<div className="container">
			<div className="row">
			  <div className="col-12 mx-auto mt-3">
			    <h2 className="text-center">TRANSACTIONS</h2>
			    <hr/>
			  </div>
			</div>
			<div className="row">
				<div className="col">
					<div class="accordion" id="accordionExample">
						{
							transactions.map(transaction => {
								return (
									<div class="card">
									  <div class="card-header" id="headingOne">
									    <h2 class="mb-0">
									      <button class="btn btn-link" type="button" data-toggle="collapse" data-target={"#collapseOne"+transaction._id} aria-expanded="true" aria-controls="collapseOne">
									        {transaction.dateCreated}
									      </button>
									    </h2>
									  </div>

									  <div id={"collapseOne"+transaction._id} class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
									    <div class="card-body">
									      <table className="table">
									      	<tr>
									      		<th className="text-right">Booking Code:</th>
									      		<td>{transaction.bookingCode}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Customer Name:</th>
									      		<td>{transaction.userId}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Reserved Room:</th>
									      		<td>{transaction.roomId}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Reserved Date:</th>
									      		<td>{transaction.reservationDate}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Time In:</th>
									      		<td>{transaction.timeIn}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Time Out:</th>
									      		<td>{transaction.timeOut}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right w-50">Payment Mode:</th>
									      		<td>{transaction.paymentMode}</td>
									      	</tr>
									      	<tr>
									      		<th className="text-right">Status:</th>
									      		<td>
									      			{transaction.status}
									      			{
									      				localStorage.getItem('role') == 'admin' ?
									      				<Fragment>
											      			<select name="status" id="status" className="form-control" onChange={handleChangeSelected}>
											      				<option value="Booked">Booked</option>
											      				<option value="Cancelled">Cancelled</option>
											      			</select>
											      			<button className="btn" onClick={handleEditTransaction}>UPDATE</button>
											      			</Fragment> : ""
									      			}
									      		</td>
									      	</tr>
									      </table>
									    </div>
									  </div>
									</div>
								)
							})
						}

					</div>
				</div>
			</div>

		</div>

	)
}

export default TransactionForm;

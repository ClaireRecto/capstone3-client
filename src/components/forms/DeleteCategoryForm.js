import React, {useState} from 'react';

const DeleteCategory = ({categories}) => {
	const [selectedCategory,setSelectedCategory] = useState({
		name: null,
		id : null
	})

	const handleChangeSelected = (e) => {
		let categorySelected = categories.find(category=>{
			return category._id === e.target.value
		})

		setSelectedCategory({
			id: e.target.value,
			name : categorySelected.name

		})
	}

	const handleDeleteCategory = e => {
		e.preventDefault();
		fetch("https://bookingking.herokuapp.com/categories/"+selectedCategory.id,{
			method: "DELETE",
			headers : {
				"Content-Type" : "application/json",
				Authorization : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => {console.log(result);window.location.href = "/admin"})
	}
	return(
		<form action="" onSubmit={handleDeleteCategory}>
			<label htmlFor="selectcategory-delete">Select Category Name:</label>
			<select onChange={handleChangeSelected} name="category" id="selectcategory-delete" className="custom-select">
				<option value="" disabled selected>Select Category</option>
				{
					categories.map(category => {
						return (
						<option value={category._id}>{category.name}</option>
						)
					})
				}
			</select>
			<button className="btn mt-2 w-100">DELETE</button>
		</form>
	)
}

export default DeleteCategory;

import React,{useState} from 'react';

const AddCategory = () => {
	const [formData,setFormData] = useState("");

	const handleCategoryNameChange = (e) => {
		setFormData(e.target.value)
	};
	const handleAddCategory = (e) => {
		e.preventDefault()
		let url ="https://bookingking.herokuapp.com/categories/";
		let data = {name: formData};
		let token = localStorage.getItem('token');
		fetch(url,{
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type" : "application/json",
				"Authorization" : token
			}
		})
		.then(data => data.json())
		.then(category => {console.log(category);window.location.href = "/admin"})
	};
	return(
		<form action="" onSubmit={handleAddCategory}>
			<label htmlFor="name">Name:</label>
			<input 
				type="text" 
				className="form-control" 
				name="name" 
				id="name"
				onChange={handleCategoryNameChange}
			/>
			<button className="btn mt-2 w-100">ADD</button>
		</form>
	)
}

export default AddCategory;

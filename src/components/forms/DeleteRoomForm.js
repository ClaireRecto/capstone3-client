import React, {useState} from 'react';

const DeleteRoom = ({rooms}) => {
	const [selectedProduct,setSelectedProduct] = useState({
		id : null
	})

	const handleChangeSelected = (e) => {
		setSelectedProduct({
			id: e.target.value,

		})
	}

	const handleDeleteRoom = e => {
		e.preventDefault();
		fetch("https://bookingking.herokuapp.com/rooms/"+selectedProduct.id,{
			method: "DELETE",
			headers : {
				Authorization : localStorage.getItem('token')
			}
		})
		.then(data => data.json())
		.then(result => {console.log(result);window.location.href = "/admin"})
	}
	return(
		<form action="" onSubmit={handleDeleteRoom}>
			<label htmlFor="selectproduct-delete">Select Product Name:</label>
			<select name="category" id="selectproduct-delete" className="custom-select" onChange={handleChangeSelected}>
				<option value="" disabled selected>Select Product</option>
				{
					rooms.map(room => {
						return (
						<option value={room._id}>{room.name}</option>
						)
					})
				}
			</select>
			<button className="btn mt-2 w-100">DELETE</button>
		</form>
	)
}

export default DeleteRoom;

import React from 'react';
import AddRoomForm from './forms/AddRoomForm';
import EditRoomForm from './forms/EditRoomForm';
import AddCategoryForm from './forms/AddCategoryForm';
import EditCategoryForm from './forms/EditCategoryForm';
import DeleteCategoryForm from './forms/DeleteCategoryForm';
import DeleteRoomForm from './forms/DeleteRoomForm';

const AdminPanel = ({categories, handleChangeCategoriesStatus, categoriesStatus, rooms}) => {
	// console.log(products)
	return(
		<div className="container">
			<div className="row">
				<div className="col-12 mx-auto mt-3">
					<h2 className="text-center">ADMIN PANEL</h2>
					<hr/>
				</div>
			</div>
			<div className="row my-4">
				<div className="col-12 col-md-4 p-3">
					<div className="p-3 rounded h-100 admindiv">
						<h3 className="text-center">ADD CATEGORY</h3>
						<AddCategoryForm/>
					</div>
				</div>
				<div className="col-12 col-md-4 p-3">
					<div className="p-3 rounded h-100 admindiv">
						<h3 className="text-center">EDIT CATEGORY</h3>
						<EditCategoryForm 
							categories={categories}
							handleChangeCategoriesStatus={handleChangeCategoriesStatus}
							categoriesStatus={categoriesStatus}
						/>
					</div>
				</div>
				<div className="col-12 col-md-4 p-3">
					<div className="p-3 rounded h-100 admindiv">
						<h3 className="text-center">DELETE CATEGORY</h3>
						<DeleteCategoryForm 
							categories={categories}
							
						/>
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col-12 col-md-6 mb-4">
					<div className="p-3 rounded h-100 admindiv">
						<h3 className="text-center">ADD ROOM</h3>
						<AddRoomForm categories={categories}/>
					</div>
				</div>
				<div className="col-12 col-md-6 mb-4">
					<div className="p-3 rounded h-100 admindiv">
						<h3 className="text-center">EDIT ROOM</h3>
						<EditRoomForm categories={categories} rooms={rooms}/>
					</div>
				</div>
				<div className="col-12">
					<div className="p-3 rounded admindiv">
						<h3 className="text-center">DELETE ROOM</h3>
						<DeleteRoomForm rooms={rooms}/>
					</div>
				</div>
				
			</div>
		</div>
	);
}

export default AdminPanel;

import React, {useState} from 'react';
import {Link} from 'react-router-dom';

const Catalog = ({rooms}) => {
	const [selectedRoom,setSelectedRoom] = useState({
		id : null
	})

	const handleOnClick = (id) => {
		setSelectedRoom({
			id : id
		})
		fetch("https://bookingking.herokuapp.com/rooms/"+id, {
					headers : {
						"Content-Type" : "application/json",
						"Authorization" : localStorage.getItem('token')
					}
				})
				.then(data => data.json())
				.then(result => {
					window.location.href = "/booking"
				})
	}
	return(
		<div className="container">
			<div className="row">
			  <div className="col-12 mx-auto mt-3">
			    <h2 className="text-center">OUR ROOMS</h2>
			    <hr/>
			  </div>
			</div>
			<div className="row mt-3">
				{
					rooms.map(room => (
					<div className="col-3 p-2" key={room.id} >
						<div className="card h-100">
							<img src={"https://bookingking.herokuapp.com" + room.image} alt="" className="img-fluid"/>
							<div className="card-body">
								<button type="button" class="btn w-100" data-toggle="modal" data-target={"#exampleModal"+room._id}>
								  {room.name}
								</button>
								<small>{room.categoryId}</small>

								
								<div class="modal fade" id={"exampleModal"+room._id} tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
								    <div class="modal-content px-3">
								    	<div className="row">	
								    		<div className="col-7 p-0">
								    			<img src={"https://bookingking.herokuapp.com" + room.image} alt="" className="img-fluid w-100 h-100"/>
								    		</div>
								    		<div className="col-5 p-3">
								    				<h5 className="modal-info">{room.name}</h5>
								    				<p className="modal-info">{room.categoryId}</p>
								    				<p className="modal-info">MAX {room.capacity} PAX</p>
								    				<p className="modal-info">&#8369; {room.price}/HOUR</p>
								    				<p className="modal-info">{room.description}</p>
								    				{
								    					localStorage.getItem('role') != 'admin' ?
								    					<a href="/booking" className="btn w-100">Book Now!</a> : ""
								    				}
								    		</div>
								    	</div>

								    </div>
								  </div>
								</div>
							</div>
						</div>
					</div>

					))
				}

			</div>
		</div>
	);
}

export default Catalog;

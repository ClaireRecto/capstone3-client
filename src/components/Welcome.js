import React from 'react';

const Welcome = () => {
	return(
		<div className="container-fluid">
			<div className="row">
				<div className="col mx-auto text-center" id="welcome">
				</div>
			</div>
			<div className="row px-5">
				<div className="col-12 col-md-4 p-5">
					<div class="card">
					  <img class="card-img-top" src="https://thumbs.dreamstime.com/b/microphone-yellow-background-curly-cable-167182436.jpg" alt="Card image cap"/>
					  <div class="card-body text-center">
					    <h6 class="card-title">ABOUT US</h6>
					    <a href="#" class="btn btn-primary">GO</a>
					  </div>
					</div>
				</div>
				<div className="col-12 col-md-4 p-5">
					<div class="card">
					  <img class="card-img-top" src="https://thumbs.dreamstime.com/b/microphone-yellow-background-curly-cable-167182436.jpg" alt="Card image cap"/>
					  <div class="card-body text-center">
					    <h6 class="card-title">OUR ROOMS</h6>
					    <a href="#" class="btn btn-primary">GO</a>
					  </div>
					</div>
				</div>
				<div className="col-12 col-md-4 p-5">
					<div class="card">
					  <img class="card-img-top" src="https://thumbs.dreamstime.com/b/microphone-yellow-background-curly-cable-167182436.jpg" alt="Card image cap"/>
					  <div class="card-body text-center">
					    <h6 class="card-title">CONTACT US</h6>
					    <a href="#" class="btn btn-primary">GO</a>
					  </div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Welcome;
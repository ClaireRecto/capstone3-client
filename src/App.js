import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Welcome from './components/Welcome';
import Navbar from './components/layouts/Navbar';
import Footer from './components/layouts/Footer';
import Catalog from './components/Catalog';
import AdminPanel from './components/AdminPanel';
import './components/styles/style.css';

//Forms
import RegisterForm from './components/forms/RegisterForm';
import LoginForm from './components/forms/LoginForm';
import TransactionForm from './components/forms/TransactionForm';
import BookForm from './components/forms/BookForm';

function App() {

  // states
  const [rooms, setRooms] = useState([]);
  const [categories, setCategories] = useState([]);
  const [categoriesStatus, setCategoriesStatus] = useState({
    lastUpdated : null,
    status : null,
    isLoading : false
  });
  const [total, setTotal] = useState(0);
  const [transactions, setTransactions] = useState([]);

  // useEffect(()=> {
  //   let total = 0;
  //   cart.forEach(item =>{
  //     total += item.price
  //   })
  //   console.log(total)
  //   setTotal(total)
  // },[cart])

  useEffect(() => {
    fetch("https://bookingking.herokuapp.com/categories")
    .then(res => res.json())
    .then(data => {
      setCategories([...data])
      setCategoriesStatus({isLoading:false})
    })
  },[categoriesStatus.isLoading])

  useEffect( () => {
      fetch("https://bookingking.herokuapp.com/rooms")
      .then(res => res.json())
      .then(data => {
        setRooms([...data])
      })

      fetch("https://bookingking.herokuapp.com/categories")
      .then(res => res.json())
      .then(data => {
        setCategories([...data])
      })

      fetch("https://bookingking.herokuapp.com/transactions", {
        headers : {
          "Authorization" : localStorage.getItem('token')
        }
      })
      .then(res => res.json())
      .then(data => {
        setTransactions([...data])
      })

    }, []);
  // change status methods
  const handleChangeCategoriesStatus= (status) => {
    setCategoriesStatus(status)
  }







  return (
    <Router>
      <div >
        <Navbar/>
        <Switch>
          <Route exact path="/">
            <Welcome />
          </Route>
          <Route path="/rooms">
            <Catalog 
              rooms={rooms}
              
              
            />
          </Route>
          {
            localStorage.getItem('role') == 'admin' ?
            <Route path="/admin">
              <AdminPanel 
                categories={categories} 
                handleChangeCategoriesStatus={handleChangeCategoriesStatus}
                categoriesStatus={categoriesStatus}
                rooms={rooms}
              />
            </Route> : ""
          }

          <Route path="/register">
            <RegisterForm/>
          </Route>
          <Route path="/login">
            <LoginForm />
          </Route>
          {
            localStorage.getItem('role') != 'admin' ?
              <Route path="/booking">
                <BookForm rooms={rooms}/>
              </Route> : ""
          }
          <Route path="/transactions">
            <TransactionForm transactions={transactions} />
          </Route>
        </Switch>
        <Footer/>
      </div>
    </Router>
  );
}

export default App;
